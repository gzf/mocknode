#!/bin/env node

var express = require('express');

var app = express();

var printall = require('./printall');

app.get('/qtels-sys/pay_config.do', function(req, res) {
  printall(req, res, () => {
    res.json({
      hotel_id: req.query.hotel_id
    });
  });
});

app.all('/**', function(req, res) {
  printall(req, res, () => {
    res.end('OK');
  });
});

app.listen(8081, '127.0.0.1', function() {
    console.log('Mock server started');
});
