#!/bin/env node

var fs = require('fs')
var express = require('express');
var printall = require('./printall');

var app = express();

app.post('/hotels/ota/OTA_HotelResNotif', function(req, res) {
  res.setHeader("content-type", "text/xml");
  fs.createReadStream("OTA_HotelResNotifRQ.xml").pipe(res);
  //res.sendFile('OTA_HotelResNotif.xml');
});

app.post('/hotels/ota/OTA_HotelResModifyNotif', function(req, res) {
  res.setHeader("content-type", "text/xml");
  fs.createReadStream("OTA_HotelResModifyNotifRQ.xml").pipe(res);
  //res.sendFile('OTA_HotelResModifyNotifRQ.xml');
});

app.all('/**', function(req, res) {
  printall(req, res, () => {
    res.setHeader("content-type", "text/xml");
    res.end('<OK/>');
  });
});

app.listen(8081, '127.0.0.1', function() {
  console.log('Mock server for Booking started');
});
