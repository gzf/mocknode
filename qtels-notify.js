#!/bin/env node

var express = require('express');

var app = express();

var printall = require('./printall');

app.post('/json', function(req, res) {
  printall(req, res, () => {
    res.json({
      status: 0,
      error_msg: 'retry for test'
    });
  });
});

app.get('/rest', function(req, res) {
  printall(req, res, () => {
    res.end('0');
  });
});

app.post('/rest', function(req, res) {
  printall(req, res, () => {
    res.end('0');
  });
});

app.all('/**', function(req, res) {
  printall(req, res, () => {
    res.end('0');
  });
});

app.listen(8081, '127.0.0.1', function() {
    console.log('Qtels notity server started');
});
