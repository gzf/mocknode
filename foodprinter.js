#!/bin/env node

var net = require('net');

net.createServer(function (socket) {
  var length = 0;
  var client = socket.remoteAddress + ":" + socket.remotePort;
  console.log('new client: ' + client);

  socket.on('data', function (data) {
    length += data.length;
    console.log(data.toString('hex'));
    if (length === 3 &&
        data[0] === 0x10 &&
        data[1] === 0x04 &&
        data[2] === 0x01) { // 查询状态
        socket.write(Buffer.from([0]));
      //socket.destroy();
    }
  });

  socket.on('end', function () {
    console.log('disconnected: ' + client);    
    console.log('totally received ' + length + ' bytes');
  });
}).listen(9100);
