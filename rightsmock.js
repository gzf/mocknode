#!/bin/env node

var express = require('express');

var app = express();

app.get('/search/healthcheck', function(req, res) {
    console.log('Request: ' + req.url);
    res.json({
		response: 'good'
    });
});

app.get('/search/**', function(req, res) {
    console.log('Request: ' + req.url);
    setTimeout(function() {
        res.json({
            hits: {
                hits: [
                    { _id: 'aaa' },
                    { _id: 'bbb' }
                ]
            }
        });
    }, 500);
});

app.get('/receipt/healthcheck', function(req, res) {
    var hdr;

    console.log('Request: ' + req.url);
    console.log('Headers: ');
    for (hdr in req.headers)
        console.log('\t' + hdr + " = " + req.headers[hdr]);
    
    res.send('ack');
});

app.get('/receipt/**', function(req, res) {
    var hdr;

    console.log('Request: ' + req.url);
    console.log('Headers: ');
    for (hdr in req.headers)
        console.log('\t' + hdr + " = " + req.headers[hdr]);
    
    setTimeout(function() {
        res.json({
            status: {
                status: 'success',
                code: '200'
            }
        });
    }, 500);
});

app.listen(8081, '127.0.0.1', function() {
    console.log('Mock server for RightsService started');
});
