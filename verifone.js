#!/bin/env node

var net = require('net');

net.createServer(function (socket) {
  var client = socket.remoteAddress + ":" + socket.remotePort;
  console.log('new client: ' + client);

  var len = 0, cur = 0;
  var json = '';

  function readjson(data, offset) {
    var remains = len - cur;
    var available = data.length - offset;
    if (available === 0)
      return;
    var max = remains > available ? available : remains;
    json += data.toString('utf8', offset, offset + max);
    cur += max;
    if (cur === len) {
      console.log('package: ' + json);
      var payload = new Buffer(json);
      var header = new Buffer(2);
      header.writeUInt16BE(payload.length);
      socket.write(Buffer.concat([header, payload]));
      socket.destroy();
    }
  }

  socket.on('data', function (data) {
    console.log('received ' + data.length + 'bytes');
    if (!len) { // start
      len = data.readUInt16BE(0);
      console.log('package started, length: ' + len);
      json = '';
      cur = 0;
      readjson(data, 2);
    }
    else
      readjson(data, 0);
  });

  socket.on('end', function () {
    console.log('disconnected: ' + client);    
  });
}).listen(6039);
