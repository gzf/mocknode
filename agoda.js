#!/bin/env node

var fs = require('fs')
var express = require('express');

var app = express();

var printall = require('./printall');

app.post('/api/ari', function(req, res) {
  printall(req, res, () => {
    res.setHeader("content-type", "text/xml");
    fs.createReadStream("agoda-ok-result.xml").pipe(res);
  });
});

app.post('/api/bookings/search', function(req, res) {
  printall(req, res, () => {
    res.setHeader("content-type", "text/xml");
    fs.createReadStream("agoda-search.xml").pipe(res);
  });
});

app.all('/**', function(req, res) {
  printall(req, res, () => {
    res.end('OK');
  });
});

app.listen(8081, '127.0.0.1', function() {
    console.log('Agoda mock server started');
});
