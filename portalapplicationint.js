#!/bin/env node

var express = require('express');

var app = express();

app.get('/portalapplicationint/*', function(req, res) {
    var hdr;

    console.log('Request: ' + req.url);
    console.log('Headers: ');
    for (hdr in req.headers)
        console.log('\t' + hdr + " = " + req.headers[hdr]);
    
    res.send(400, '3');
});

app.listen(8081, '127.0.0.1', function() {
    console.log('Mock server for portalapplicationint started');
});
