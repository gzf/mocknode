module.exports = function(req, res, next) {
  console.log('Version: ' + req.httpVersion);
  console.log('Method: ' + req.method);
  console.log('Path: ' + req.url);
  console.log('Headers: ');
  for (var hdr in req.headers)
    console.log('\t' + hdr + " = " + req.headers[hdr]);
  if (req.method === 'POST' || req.method === 'PUT') {
    var body = '';
    req.on('data', function (data) {
      body += data;
    });
    req.on('end', function () {
      console.log('Body:\n' + body);
      next();
    });
  }
  else
    next();
};
