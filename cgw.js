#!/bin/env node

var express = require('express');

var app = express();

app.get('/adr/1.0/cloudgateway/config/Gateway_config', function(req, res) {
    res.set('Content-Type', 'application/xml');
    res.sendfile('_globalConfig.xml');
});

app.get('/tokengateway/v1/internal/getgenericvalidationinfo', function(req, res) {
    console.log('token: ' + req.headers['authorization']);
    res.set('Content-Type', 'application/json');
    res.sendfile('userProfile.json');
});

app.get('/rights/1.0/authorize', function(req, res) {
    if (req.get('Authorization') &&
        req.get('X-Catalog-Id') &&
        req.get('X-Device-Id') &&
        req.get('X-HP-Client-Id') &&
        (req.get('X-URL') || req.get('X-Product-Id')))
        res.send(304);
    else
        res.send(403, 'HTTP 403: ACCESS DENY')
});

app.listen(8081, '0.0.0.0', function() {
    console.log('Mock server for Tableau Plugin started');
});
